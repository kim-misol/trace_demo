import React, {useEffect, useState} from 'react';
import './App.css';
import user_connection_img from './images/NFT user connection.png';
import trading_intro_img from './images/trading intro.png';

function App() {
    /*
    * ES6 destructuring 문법
    * array. object 에 있던 자료를 변수에 쉽게 담고 싶을 때 사용
    *
    * useState: 변수 대신에 쓸 수 있는 데이터 저장 공간
    * state 장점 :
    * react 를 웹앱처럼 쓸 수 있다
    * 데이터가 수정될 때 데이터를 담고있는 html 이 자동으로 재랜더링 된다
    * 스무스한 동작
    * 새로고침을 하지 않아도 된다.
    * */
    const [screenSize, getDimension] = useState({
        dynamicWidth: window.innerWidth,
        dynamicHeight: window.innerHeight
    });
    const setDimension = () => {
        getDimension({
            dynamicWidth: window.innerWidth,
            dynamicHeight: window.innerHeight
        })
    }

    useEffect(() => {
        window.addEventListener('resize', setDimension);

        return (() => {
            window.removeEventListener('resize', setDimension);
        })
    }, [screenSize])
    let userConnectionImageStyle = {width: screenSize.dynamicWidth * 3 / 16, paddingTop: '80px', paddingBottom: '30px'}
    let tradingIntroImageStyle = {width: screenSize.dynamicWidth, paddingTop: '80px', paddingBottom: '30px'}

    let topStyle = {fontSize: '26px', color: '#636DBE', paddingTop: '16px'}
    let fontColorRed = {color: '#D13838'}
    // function a() {
    //     return 'TRACE는 ...'
    // }

    return (
        <div className="App">
            <div className="white-nav">
                <div>TRACE</div>
            </div>
            <img src={user_connection_img} alt="user connection" style={userConnectionImageStyle}/>
            <div className="vision">내 투자 전략, <span style={ fontColorRed }>NFT</span> 자산이 되다</div>
            <div className="body">TRACE 는 <span style={ fontColorRed }>개인의 투자 전략</span>을
                <span style={ fontColorRed }>토근화</span> 하여 <span style={ fontColorRed }>거버넌스</span>를 형성하고<br/>
                <span style={ fontColorRed }>수익을 창출</span>하는 마켓 플레이스 입니다.</div>
            {/*<h6>{a()}</h6>*/}

            <img src={trading_intro_img} alt="user connection" style={tradingIntroImageStyle}/>
            <div className="body"><span style={topStyle}>Top 6</span> Trading NFT Ranking</div>
            <div className="body"><span style={topStyle}>Top 3</span> 기부 크리에이터 Ranking</div>


        </div>
    );

}

export default App;
