import pandas as pd
import plotly.express as px
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView

from . import cf
from .config import connect_post, session_scope_with_engine


class ResultAPIView(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request):
        data = request.session.get('result')
        return Response(data)


def result_detail(request):
    context = {}
    return render(request, 'algorithms/chart.html', context)


# 메인 페이지 로드 시 호출
def index(request):
    database_summary_list = get_database_list_summary()
    database_list = get_database_list()

    res = {'database_summary_list': database_summary_list, 'database_list': database_list}
    return render(request, 'simulation_list.html', res)


# simulation 페이지 로드 시 호출
def simulation(request):
    if request.method == 'GET':
        db_name = request.GET['db_name']
        jango_df = get_jango_df(db_name)
        da_df = get_da_df(db_name)
        fig = px.line(jango_df, x="created_at_kst", y=['btc_acc_rate', 'acc_rate', 'btc_mdd', 'mdd'],
                      title='알고리즘과 btc 결과 비교')
        fig.data[2].line.color = "#81c995"
        fig.data[3].line.color = "#f28b82"
        graph = fig.to_html(full_html=False, default_height='100%', default_width='100%', div_id='graphId')
    else:
        db_name = ""
        graph = None
    database_list = get_database_list()

    jango_df['created_at_utc'] = jango_df['created_at_utc'].dt.strftime('%Y-%m-%d')
    jango_df['today_buy_stop_time_kst'] = pd.to_datetime(jango_df['today_buy_stop_time_kst']).dt.strftime('%Y-%m-%d %H:%M')
    jango_df['today_earning_rate'] = jango_df['today_earning_rate'].round(2)
    da_df['sell_rate'] = da_df['sell_rate'].round(2)
    da_df['valuation_price'] = da_df['valuation_price'].round(0)
    da_df['valuation_profit'] = da_df['valuation_profit'].round(0)
    da_df['buy_date'] = da_df['buy_date'].dt.strftime('%Y-%m-%d %H:%M')
    da_df['sell_date'] = da_df['sell_date'].dt.strftime('%Y-%m-%d %H:%M')

    jango_df = jango_df.fillna(0)
    da_df = da_df.fillna(0)

    res = {
        'database_list': database_list,
        'db_name': db_name,
        'graph': graph,
        'jango_data': jango_df.T.to_dict().values(),
        'da_data': da_df.T.to_dict().values(),
    }
    return render(request, 'simulation.html', res)


# 잔고 데이터 가져오기
def get_jango_df(db_name):
    db_info = cf.db_info['simulator']
    db_info['name'] = db_name
    db_engine = connect_post(db_info)
    jango_list, da_list = get_table(db_engine)
    if len(jango_list) > 0:
        jango_table = jango_list[0]
        sql = f"select * from {jango_table} order by created_at_kst asc"
        res = pd.read_sql(sql, db_engine)
    else:
        res = []
    return res


# all_item_db 데이터 가져오기
def get_da_df(db_name):
    db_info = cf.db_info['simulator']
    db_info['name'] = db_name
    db_engine = connect_post(db_info)
    jango_list, da_list = get_table(db_engine)
    if len(da_list) > 0:
        da_table = da_list[0]
        sql = f"SELECT * FROM {da_table} order by buy_date asc"
        res = pd.read_sql(sql, db_engine)
    else:
        res = []
    return res


# 시뮬레이션 최종 결과 데이터
def get_simul_result_summary(db_name):
    db_info = cf.db_info['simulator']
    db_info['name'] = db_name
    db_engine = connect_post(db_info)
    jango_list, da_list = get_table(db_engine)
    result = {}
    if len(jango_list) > 0:
        jango_table = jango_list[0]
        # 전 시뮬레이션 mdd 값을 절대값으로 업데이트 하기 위한 쿼리 실행 (실행 완료)
        # with session_scope_with_engine(db_engine) as session:
        #     sql = f"UPDATE {jango_table} SET mdd = abs(mdd), btc_mdd = abs(btc_mdd);"
        #     session.execute(sql)
        sql = f"SELECT * FROM {jango_table} ORDER BY created_at_utc DESC"
        jango_df = pd.read_sql(sql, db_engine)
        if len(jango_df) > 0:
            jango_df['created_at_utc'] = jango_df['created_at_utc'].dt.strftime('%Y-%m-%d')
            result = dict(jango_df.iloc[0])
            total_profitcut_count = int(result['total_profitcut_count'])
            total_losscut_count = int(result['total_losscut_count'])

            if total_profitcut_count + total_losscut_count > 0:
                profitcut_rate = total_profitcut_count / (total_profitcut_count + total_losscut_count) * 100
                result[
                    'profitcut_rate'] = f"{round(profitcut_rate, 2)} ({total_profitcut_count} : {total_losscut_count})"
            else:
                result['profitcut_rate'] = None
            jango_df = jango_df.fillna(0)
            result['simul_start_date'] = jango_df.tail(1).created_at_utc.item()
            result['first_invest_amount'] = jango_df.tail(1).total_invest.item() - jango_df.tail(1).sum_valuation_profit.item()
            # result['mdd'] = abs(result['mdd'])
            # result['btc_mdd'] = abs(jango_df.tail(1).btc_mdd.item())
    if not result:
        result = {
            "acc_rate": None,
            "mdd": None,
            "btc_acc_rate": None,
            "btc_mdd": None,
            "sum_valuation_profit": None,
            "first_invest_amount": None,
            "total_losscut_count": None,
            "total_profitcut_count": None,
            "created_at_kst": None,
            "simul_start_date": None}
    return result


# 데이터베이스 시뮬레이션 결과 데이터 response
def get_database_list_summary():
    db_info = cf.db_info['simulator']
    db_engine = connect_post(db_info)
    with session_scope_with_engine(db_engine) as session:
        sql = "SELECT datname FROM pg_database;"
        rows = session.execute(sql).fetchall()
    database_list = [row[0] for row in rows]
    database_list = [db_name for db_name in database_list if 'simulator' in db_name]
    result = []
    idx = 0
    for db_name in database_list:
        idx += 1
        summary = get_simul_result_summary(db_name)
        algorithm_name = db_name.replace('simulator', '').split('(')[0]
        data = {'index': idx, 'db_name': db_name, 'algorithm_name': algorithm_name, 'summary': summary}
        if summary['simul_start_date'] is None:
            result = result + [data]
        else:
            result = [data] + result

    return result


# 시뮬레이션 데이터베이스의 테이블 리스트
def get_table(db_engine):
    jango_list = [jango_table for jango_table in db_engine.table_names() if 'jango_data' in jango_table]
    da_list = [da_table for da_table in db_engine.table_names() if 'all_item_db' in da_table]
    return jango_list, da_list


# 데이터베이스 리스트
def get_database_list():
    db_info = cf.db_info['simulator']
    db_engine = connect_post(db_info)
    with session_scope_with_engine(db_engine) as session:
        sql = "SELECT datname FROM pg_database;"
        rows = session.execute(sql).fetchall()
    result = [row[0] for row in rows]
    result = [r for r in result if 'simulator' in r]
    return result


# 알고리즘 리스트
def get_algorithm_list():
    db_info = cf.db_info['simulator']
    db_engine = connect_post(db_info)
    with session_scope_with_engine(db_engine) as session:
        sql = "SELECT datname FROM pg_database;"
        rows = session.execute(sql).fetchall()
    result = [row[0] for row in rows]
    result = [r for r in result if 'simulator' in r]
    result = [r.replace('simulator', '').split('(')[0] for r in result]
    return result
