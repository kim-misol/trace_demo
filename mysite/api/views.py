from rest_framework.decorators import api_view
from rest_framework.response import Response
import pandas as pd
from . import cf
from .config import connect_post, session_scope_with_engine
import json

# Create your views here.
@api_view(['GET'])
def testAPI(request):
    db_name = 'simulator10004_55614(20210101~)_limit'
    jango_df = get_da_df(db_name)

    return Response(str(jango_df))


def get_jango_df(db_name):
    db_info = cf.db_info['simulator']
    db_info['name'] = db_name
    db_engine = connect_post(db_info)
    jango_list, da_list = get_table(db_engine)
    if len(jango_list) > 0:
        jango_table = jango_list[0]
        sql = f"select * from {jango_table} order by created_at_kst asc"
        res = pd.read_sql(sql, db_engine)
    else:
        res = []
    return res


def get_da_df(db_name):
    db_info = cf.db_info['simulator']
    db_info['name'] = db_name
    db_engine = connect_post(db_info)
    jango_list, da_list = get_table(db_engine)
    if len(da_list) > 0:
        da_table = da_list[0]
        sql = f"select * from {da_table} order by buy_date asc"
        res = pd.read_sql(sql, db_engine)
    else:
        res = []
    return res


@api_view(['GET'])
def get_database_list_summary(request):
    db_info = cf.db_info['simulator']
    db_engine = connect_post(db_info)
    with session_scope_with_engine(db_engine) as session:
        sql = "SELECT datname FROM pg_database;"
        rows = session.execute(sql).fetchall()
    database_list = [row[0] for row in rows]
    database_list = [db_name for db_name in database_list if 'simulator' in db_name]
    result = []
    idx = 0
    for db_name in database_list:
        idx += 1
        summary = _get_simul_result(db_name)
        data = {'index': idx, 'db_name': db_name, 'summary': summary}
        result.append(data)
    # data_df =
    return Response(result)


@api_view(['GET'])
def get_database_list(request):
    db_info = cf.db_info['simulator']
    db_engine = connect_post(db_info)
    with session_scope_with_engine(db_engine) as session:
        sql = "SELECT datname FROM pg_database;"
        rows = session.execute(sql).fetchall()
    result = [row[0] for row in rows]
    result = [r for r in result if 'simulator' in r]
    return Response(result)

@api_view(['GET'])
def get_algorithm_list(request):
    db_info = cf.db_info['simulator']
    db_engine = connect_post(db_info)
    with session_scope_with_engine(db_engine) as session:
        sql = "SELECT datname FROM pg_database;"
        rows = session.execute(sql).fetchall()
    result = [row[0] for row in rows]
    result = [r for r in result if 'simulator' in r]
    result = [r.replace('simulator', '').split('(')[0] for r in result]
    return Response(result)


@api_view(['GET'])
def get_simul_result_summary(request):
    db_name = 'simulator70_9961(20211101~)_limit'
    db_info = cf.db_info['simulator']
    db_info['name'] = db_name
    db_engine = connect_post(db_info)
    jango_list, da_list = get_table(db_engine)
    if len(jango_list) > 0:
        jango_table = jango_list[0]
        with session_scope_with_engine(db_engine) as session:
            sql = f"SELECT acc_rate, mdd, btc_acc_rate, btc_mdd FROM {jango_table};"
            res = session.execute(sql).fetchall()
            res = res[0] if len(res) > 0 else res
    else:
        res = {
            "acc_rate": None,
            "mdd": None,
            "btc_acc_rate": None,
            "btc_mdd": None,
            "created_at_kst": None}

    return Response(res)


@api_view(['GET'])
def get_simul_result(request):
    db_name = 'simulator10004_55614(20211001~)_limit'
    db_info = cf.db_info['simulator']
    db_info['name'] = db_name
    db_engine = connect_post(db_info)
    jango_list, da_list = get_table(db_engine)
    if len(jango_list) > 0:
        jango_table = jango_list[0]
        with session_scope_with_engine(db_engine) as session:
            sql = f"SELECT acc_rate, mdd, btc_acc_rate, btc_mdd FROM {jango_table};"
            res = session.execute(sql).fetchall()
            res
    else:
        res = []
    return Response(res)


@api_view(['GET'])
def get_jango(request):
    db_name = 'simulator10004_55614(20211001~)_limit'
    db_info = cf.db_info['simulator']
    db_info['name'] = db_name
    db_engine = connect_post(db_info)
    jango_list, da_list = get_table(db_engine)
    if len(jango_list) > 0:
        jango_table = jango_list[0]
        with session_scope_with_engine(db_engine) as session:
            sql = f"SELECT * FROM {jango_table};"
            res = session.execute(sql).fetchall()
    else:
        res = []
    return Response(res)


@api_view(['GET'])
def get_da(request):
    db_name = 'simulator10004_55614(20210101~)_limit'
    db_info = cf.db_info['simulator']
    db_info['name'] = db_name
    db_engine = connect_post(db_info)
    jango_list, da_list = get_table(db_engine)
    if len(da_list) > 0:
        da_table = da_list[0]
        with session_scope_with_engine(db_engine) as session:
            sql = f"SELECT * FROM {da_table};"
            res = session.execute(sql).fetchall()
    else:
        res = []
    return Response(res)


def _get_simul_result(db_name):
    db_info = cf.db_info['simulator']
    db_info['name'] = db_name
    db_engine = connect_post(db_info)
    jango_list, da_list = get_table(db_engine)
    result = {}
    if len(jango_list) > 0:
        jango_table = jango_list[0]
        with session_scope_with_engine(db_engine) as session:
            sql = f"SELECT acc_rate, mdd, btc_acc_rate, btc_mdd, created_at_kst FROM {jango_table} ORDER BY created_at_kst DESC;"
            res = session.execute(sql).fetchall()
            if len(res) > 0:
                result = dict(res[0])
                result['simul_start_date'] = res[-1].created_at_kst
    if not result:
        result = {
            "acc_rate": None,
            "mdd": None,
            "btc_acc_rate": None,
            "btc_mdd": None,
            "created_at_kst": None,
            "simul_start_date": None}
    return result


def get_table(db_engine):
    jango_list = [jango_table for jango_table in db_engine.table_names() if 'jango_data' in jango_table]
    da_list = [da_table for da_table in db_engine.table_names() if 'all_item_db' in da_table]
    return jango_list, da_list
