# api/urls.py

from django.urls import path, include
from .views import *

urlpatterns = [
    path("test/", testAPI),
    path("get_database_list/", get_database_list),
    path("get_simul_result/", get_simul_result),
    path("get_simul_result_summary/", get_simul_result_summary),
    path("get_database_list_summary/", get_database_list_summary),
    path("get_da/", get_da),
    path("get_jango/", get_jango),
    path("get_algorithm_list/", get_algorithm_list),
]