"""
사용자가 정의한 setting(settings.ini)에 따라 설정 값을 저장
"""
from time import sleep
import sqlalchemy
from . import cf

from sqlalchemy.exc import OperationalError
from contextlib import contextmanager
from sqlalchemy.orm import sessionmaker


@contextmanager
def session_scope_with_engine(engine_bot):
    """Provide a transactional scope around a series of operations."""
    for i in range(30):
        engine_bot_session = sessionmaker(bind=engine_bot, autoflush=False, autocommit=False)
        session = engine_bot_session()
        try:
            session.execute('select 1')
            yield session
            session.commit()
            break
        except OperationalError as e:
            print(i)
            sleep(0.2)
        except RuntimeError as e:
            print(i)
            sleep(0.2)
        except Exception as e:
            session.rollback()
            raise
            break  # 확인 필요
        finally:
            session.close()


def session_maker(engine_bot, cnt=False):
    try:
        engine_bot_session = sessionmaker(bind=engine_bot, autoflush=False, autocommit=False)
    except Exception as e:
        sleep(0.2)  # TODO: loop 에 빠져서 딜레이 걸릴 수 있는 케이스 고려
        session_maker(engine_bot, cnt=cnt)
    return engine_bot_session


def connect_post(db_info):
    user, password, host, port, db = db_info['id'], db_info['passwd'], db_info['ip'], db_info['port'], db_info['name']
    url = f'postgresql://{user}:{password}@{host}:{port}/{db}'
    con = sqlalchemy.create_engine(url, client_encoding='utf8', max_overflow=cf.limit_sqlalchemy_max_connection_num)  # max_overflow는 허용된 connection 수 이상이 들어왔을 때, 최대 얼마까지는 허용하겠다는 것
    return con

